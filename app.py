import threading
import time
import tkinter as tk
from tkinter import ttk
from pytube import YouTube, Playlist
import subprocess
import asyncio

docker_image = "yt2mp3:v2"
font = "Yu Gothic UI"


def on_entry_change(label_to_update, textfield, *args):
    title = get_video_info(textfield.get())
    label_to_update.config(text=f"{title}", fg="blue")


def get_video_info(youtube_url):
    try:
        if 'watch?v=' in youtube_url:
            # Individual video URL
            video = YouTube(youtube_url)
            return [video.title]

        elif 'playlist?list=' in youtube_url:
            # Playlist URL
            playlist = Playlist(youtube_url)
            video_titles = [video.title for video in playlist.videos]
            return video_titles

        else:
            return []

    except Exception as e:
        return f"An error occurred: {str(e)}"


async def download_yt_video():
    try:
        videourl = textfield_dl_link.get()
        targetDir = textfield_dl_dir.get()

        start_time = time.time()
        subprocess_command = ['docker', 'run', '--rm', "-v", targetDir +
                              ":/app/results", "--name", "yt2mp3", docker_image, videourl]
        process = await asyncio.create_subprocess_exec(
            *subprocess_command,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE
        )

        collected_output = ""
        while True:
            output = await process.stdout.readline()
            if not output:
                break

            output_text = output.decode("utf-8")
            collected_output += output_text
            elapsed_time = time.time() - start_time
            label_time_taken.config(text=f"{elapsed_time:.2f}s")

        await process.wait()

        elapsed_time = time.time() - start_time
        label_time_taken.config(text=f"{elapsed_time:.2f}s")

        output_lines = []
        is_output_section = False

        for line in collected_output.split("\n"):
            if line.startswith("OUTPUT_MARKER: "):
                output_lines.append(line[len("OUTPUT_MARKER: "):])
                is_output_section = True
            elif is_output_section:
                continue
        [add_downloaded_row(r) for r in output_lines]
    except Exception as e:
        print("Error", e)


def label_text_changed(label_to_update, *args):
    # Update the label's text based on the variable's value
    label_to_update.config(text="changed")


def add_downloaded_row(song_name):
    if not song_name:
        return

    new_div = tk.Frame(root, bd=1,  relief="raised", borderwidth=2)

    label_entry_song_name = tk.Label(
        new_div, text=song_name, font=(font, 10, "normal"), fg="green")

   # sv_dl.trace_add('write',  lambda *args: on_entry_change(label_entry_song_name, *args))

 #   button = tk.Button(new_div, text="🔽 Import",
 #                       font=("Helvetica", 12, "bold"),
 #                       command=lambda: download_yt_video(label_entry_song_name))
 #   button.pack(side=tk.RIGHT, padx=5)
    label_entry_song_name.pack(side=tk.LEFT, padx=5)

    new_div.pack(fill=tk.X, padx=5, pady=5)


def check_if_yt_exists_and_download():
    song_name = get_video_info(textfield_dl_link.get())
    if not song_name:
        return


    threading.Thread(target=lambda loop: loop.run_until_complete(download_yt_video()),
                         args=(asyncio.new_event_loop(),)).start()



############################################



# Create the main application window
root = tk.Tk()
root.title("YT 🔽")
root.minsize(width=600, height=800)

icon_path = "yl-dl-ui.ico"
root.iconbitmap(icon_path)


# config frame
config_frame = tk.Frame(root, width=100, height=50,
                        relief="flat", borderwidth=2)
label_dl_dir = tk.Label(config_frame, text="Download dir: ",
                        font=(font, 16, "bold"))
label_dl_dir.pack(side=tk.LEFT, padx=5)
textfield_dl_dir = tk.Entry(config_frame)
textfield_dl_dir.insert(0, "c/music/debug")
textfield_dl_dir.pack(padx=10, pady=5, side="left")
config_frame.pack(fill="x")

# start frame
start_frame = tk.Frame(root, width=100, height=50,
                    relief="flat", borderwidth=2)
label_dl_link = tk.Label(start_frame, text="Link: ",
                        font=(font, 16, "bold"))
label_dl_link.pack(side=tk.LEFT, padx=10)
label_dl_link.grid(row=0, column=0, sticky='w')

sv_dl_link = tk.StringVar()
textfield_dl_link = tk.Entry(start_frame, textvariable=sv_dl_link, width=80)
textfield_dl_link.grid(row=0, column=1, sticky='w')

sv_dl_link.trace_add(
    'write', lambda *args: check_if_yt_exists_and_download())
start_frame.pack(fill="x")

label_time_taken_text = ttk.Label(start_frame, text="Time:",
                        font=(font, 16, "bold"))
label_time_taken_text.grid(row=1, column=0, sticky='w')

label_time_taken = ttk.Label(start_frame, text="",
                        font=(font, 16, "bold"))
label_time_taken.grid(row=1, column=1, sticky='w')

separator = ttk.Separator(root, orient='horizontal')
separator.pack(fill='x', padx=10, pady=10)

# initial song
sv = tk.StringVar()

# Create a frame
frame = tk.Frame(root, padx=10, pady=10)
frame.pack()


# Start the main event loop
root.mainloop()

