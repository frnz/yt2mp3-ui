import tkinter as tk
from tkinter import ttk
import asyncio
import subprocess

async def run_subprocess_and_update_output():
    # Your subprocess logic here
    pass

async def check_if_yt_exists_and_download():
    # Your YouTube download logic here
    pass

async def start_process():
    await run_subprocess_and_update_output()
    await check_if_yt_exists_and_download()

def main():
    root = tk.Tk()
    root.title("Async Subprocess Output Example")

    button = ttk.Button(root, text="Start Process", command=start_process)
    button.pack()

    loop = asyncio.get_event_loop()
    loop.run_forever()

if __name__ == "__main__":
    main()